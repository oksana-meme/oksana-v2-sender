<?php

use App\Models\VkMessage;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vk_messages', function (Blueprint $table) {
            $table->id();

            $table->json('data');
            $table->string('status')->default(VkMessage::PENDING_STATUS);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vk_messages');
    }
};
