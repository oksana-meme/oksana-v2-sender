<?php

use App\Models\TelegramChat;
use App\Models\VkChat;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('chat_relations', function (Blueprint $table) {
            $table->foreignIdFor(VkChat::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(TelegramChat::class)->constrained()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('chat_relations');
    }
};
