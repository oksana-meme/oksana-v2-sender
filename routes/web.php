<?php

use App\Http\Controllers\VkChatController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::post('/vk/message', [VkChatController::class, 'prepareMessage']);
