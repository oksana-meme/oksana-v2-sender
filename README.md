# Docker

### Running
```shell
docker-compose up --force-recreate --build
```

### Network
```shell
docker network create -d bridge oksana-v2
```
