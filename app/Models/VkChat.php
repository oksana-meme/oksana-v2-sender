<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class VkChat extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'chat_id'
    ];

    public function tgChats(): BelongsToMany
    {
        return $this->belongsToMany(TelegramChat::class, 'chat_relations');
    }
}
