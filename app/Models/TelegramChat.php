<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TelegramChat extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'chat_id',
        'prefix'
    ];

    public function vkChats(): BelongsToMany
    {
        return $this->belongsToMany(VkChat::class, 'chat_relations');
    }
}
