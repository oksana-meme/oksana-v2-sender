<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VkMessage extends Model
{
    use HasFactory;

    protected $fillable = [
        'data',
        'status'
    ];

    public const PENDING_STATUS = 'pending';
    public const SUCCESS_STATUS = 'success';
    public const ERROR_STATUS = 'error';
}
