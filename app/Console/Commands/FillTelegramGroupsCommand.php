<?php

namespace App\Console\Commands;

use App\Services\TelegramBotService;
use Illuminate\Console\Command;

class FillTelegramGroupsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:fill-groups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill telegram groups by last updates';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        (new TelegramBotService())->fillGroups();
    }
}
