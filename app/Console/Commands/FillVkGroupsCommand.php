<?php

namespace App\Console\Commands;

use App\Services\VkBotService;
use Illuminate\Console\Command;

class FillVkGroupsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vk:fill-groups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill vk groups';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        (new VkBotService())->fillGroups();
    }
}
