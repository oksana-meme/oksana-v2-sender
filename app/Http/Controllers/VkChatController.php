<?php

namespace App\Http\Controllers;

use App\Services\TelegramSenderService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram;
use Telegram\Bot\FileUpload\InputFile;

class VkChatController extends Controller
{
    public function prepareMessage(Request $request)
    {
        $data = $request->post();

        try {
            TelegramSenderService::create($data['message'], $data['author'])
                ->setText($data['message']['text'])
                ->setPeerId($data['message']['peer_id'])
                ->setAttachments($data['message']['attachments'])
                ->send();

            return response()->json(['status' => 'success']);
        } catch (Exception $exception) {
            $error = implode('. ', [
                $exception->getFile(),
                $exception->getLine(),
                $exception->getMessage()
            ]);

            Log::error($error . '. ' . $exception->getTraceAsString());
            Log::error('Data: ' . json_encode($data));

            // Chat for errors TODO: make dynamic
            $errorChatId = "-4118972810";
            Telegram::sendDocument([
                'chat_id' => $errorChatId,
                'caption'    => "Недоразумение: \n\n" . $error,
                'document' => InputFile::createFromContents(json_encode($data), 'error-' . time() . '.json')
            ]);

            return response()->json(['status' => 'error'], 500);
        }
    }
}
