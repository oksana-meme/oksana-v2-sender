<?php

namespace App\Services;

use App\Models\VkChat;
use Exception;
use VK\Client\VKApiClient;
use VK\Exceptions\Api\VKApiMessagesChatNotExistException;
use VK\Exceptions\Api\VKApiMessagesChatUserNoAccessException;
use VK\Exceptions\Api\VKApiMessagesContactNotFoundException;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class VkBotService
{
    private VKApiClient $vk;
    private ?string $accessToken;

    public const CHAT_DOESNT_EXIST_CODE = '927';

    public function __construct()
    {
        $this->vk = new VKApiClient();
        $this->accessToken = config('vkbot.token');
    }

    public function getGroups(): array
    {
        $groups = [];

        for ($index = 1; true; $index++) {
            try {
                $groups[] = $this->getGroup(2000000000 + $index);
            } catch (Exception $e) {
                if (
                    $e->getCode() === self::CHAT_DOESNT_EXIST_CODE
                    || $e->getMessage() === 'Chat does not exist'
                ) {
                    break;
                }
            }
        }

        return $groups;
    }

    /**
     * @param int $peerId
     * @return mixed
     * @throws VKApiException
     * @throws VKApiMessagesChatNotExistException
     * @throws VKApiMessagesChatUserNoAccessException
     * @throws VKApiMessagesContactNotFoundException
     * @throws VKClientException
     */
    public function getGroup(int $peerId): mixed
    {
        $cacheId = 'vk.chats.' . $peerId;
        $expirationDate = now()->addMinutes(60);

        $group = cache()->remember($cacheId, $expirationDate, function () use ($peerId) {
            return $this->vk->messages()->getConversationsById($this->accessToken, [
                'peer_ids' => [$peerId]
            ]);
        });

        return $group['items'][0];
    }

    public function fillGroups(): void
    {
        $groups = $this->getGroups();;

        foreach ($groups as $group) {
            VkChat::query()->updateOrCreate([
                'chat_id' => $group['peer']['id'],
            ], [
                'chat_id' => $group['peer']['id'],
                'name' => $group['chat_settings']['title'],
            ]);
        }
    }
}
