<?php

namespace App\Services;

use App\Models\TelegramChat;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Objects\Update;

class TelegramBotService
{
    /**
     * @return array
     */
    public function getGroupsByUpdates(): array
    {
        $updates = Telegram::getUpdates();

        $groups = [];

        /**
         * @var Update $update
         */
        foreach ($updates as $update) {
            $chat = $update->getChat();

            $chatId = $chat->get('id');

            $groups[$chatId] = [
                'chat_id' => $chatId,
                'name' => $chat->get('title'),
            ];
        }

        return $groups;
    }

    public function fillGroups(): void
    {
        $groups = $this->getGroupsByUpdates();

        foreach ($groups as $group) {
            TelegramChat::query()->updateOrCreate([
                'chat_id' => $group['chat_id'],
            ], $group);
        }
    }
}
