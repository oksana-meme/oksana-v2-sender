<?php

namespace App\Services;

use Illuminate\Support\Arr;

class TelegramBotTextService
{
    private $message;
    private $author;

    private string $prefix = 'Мемесатор: ';

    public function __construct($message, $author)
    {
        $this->message = $message;
        $this->author = $author;
    }

    public static function create($message, $author): TelegramBotTextService
    {
        return new self($message, $author);
    }

    public function setPrefix(string $prefix): TelegramBotTextService
    {
        $this->prefix = "$prefix";

        return $this;
    }

    public function getPrefix(): string
    {
        return "<b>$this->prefix</b>";
    }

    public function getName(): string
    {
        return $this->author['first_name'] . ' ' . $this->author['last_name'];
    }

    public function getMessage(): string
    {
        if (empty($this->message['text'])) {
            return "";
        }

        return "\n\n" . htmlspecialchars($this->message['text']);
    }

    public function isWallPost(): bool
    {
        return !empty($this->message['attachments'][0]['type'])
            && !empty($this->message['attachments'][0]['wall'])
            && $this->message['attachments'][0]['type'] === 'wall';
    }

    public function getWallPostText(): string
    {
        $wall = Arr::first($this->message['attachments'], function ($attachment) {
            return $attachment['type'] === 'wall';
        });

        return $this->isWallPost()
            ? ($this->getMessage() ? "\n" : "") . self::generateWallPostText($wall['wall']['text'])
            : "";
    }

    public function generate(): string
    {
        return $this->getPrefix()
            . $this->getName()
            . $this->getMessage();
    }

    public static function generateWallPostText($text): string
    {
        return $text
            ? "\n\n<b>Пост</b>\n" . htmlspecialchars($text)
            : "";
    }

    public static function generateWallPostLink($ownerId, $wallId): string
    {
        return $ownerId && $wallId
            ? "\n\nИсточник: https://vk.com/wall{$ownerId}_{$wallId}\n\n"
            : "";
    }
}
