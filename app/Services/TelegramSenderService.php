<?php

namespace App\Services;

use App\Models\VkChat;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Objects\InputMedia\InputMediaAudio;
use Telegram\Bot\Objects\InputMedia\InputMediaDocument;
use Telegram\Bot\Objects\InputMedia\InputMediaPhoto;
use Telegram\Bot\Objects\Message;

class TelegramSenderService
{
    private $message;
    private $author;
    private $peerId;
    private $text;
    private TelegramBotTextService $captionService;

    private $part = 1;
    private $attachments;
    private $lastMessage = null;

    private $wallPostLink = '';
    private $wallPostText = '';
    private bool $isWallPost = false;


    public function __construct($message, $author)
    {
        $this->message = $message;
        $this->author = $author;

        $this->captionService = TelegramBotTextService::create($this->message, $this->author);
    }

    public static function create($message, $author): TelegramSenderService
    {
        return new self($message, $author);
    }

    public function send(): ?Message
    {
        return $this->sendMessage();
    }

    public function setPeerId($value): self
    {
        $this->peerId = $value;

        return $this;
    }

    public function setText($value): self
    {
        $this->text = $value;

        return $this;
    }

    public function setAttachments($value): self
    {
        $this->attachments = $value;

        return $this;
    }

    public function setLastMessage($value): self
    {
        $this->lastMessage = $value;

        return $this;
    }

    public function setIsWallPost(bool $value): self
    {
        $this->isWallPost = $value;

        return $this;
    }

    public function setWallPostLink($ownerId, $wallId): self
    {
        $this->wallPostLink = TelegramBotTextService::generateWallPostLink($ownerId, $wallId);

        return $this;
    }

    public function setWallPostText($value): self
    {
        $this->wallPostText = TelegramBotTextService::generateWallPostText($value);

        return $this;
    }

    private function sendMessage(): ?Message
    {
        if (empty($this->peerId)) {
            return null;
        }

        $tgChats = $this->getTgChats($this->peerId);

        $attachmentsByTypes = $this->getAttachments();

        $countOfTypes = count(array_keys($attachmentsByTypes));

        foreach ($tgChats as $tgChat) {
            foreach ($attachmentsByTypes as $type => $attachments) {
                $tgChatId = $tgChat->chat_id;

                if (empty($attachments)) {
                    continue;
                }

                $this->captionService->setPrefix($tgChat->prefix);

                if ($type === 'walls') {
                    $wallMessage = $this->message;

                    if ($countOfTypes > 1) {
                        $wallMessage['text'] = null;
                    }

                    foreach ($attachments as $wall) {
                        if (empty($wall['wall']['attachments']) && !empty($wall['wall']['text'])) {
                            $text = $this->captionService->getPrefix() . $this->captionService->getName()
                                . ($countOfTypes === 1 ? $this->captionService->getMessage() : "")
                                . TelegramBotTextService::generateWallPostText($wall['wall']['text'])
                                . TelegramBotTextService::generateWallPostLink($wall['wall']['owner_id'], $wall['wall']['id']);

                            $this->lastMessage = Telegram::sendMessage([
                                'chat_id' => $tgChatId,
                                'parse_mode' => 'html',
                                'text' => $text
                            ]);

                            continue;
                        }

                        $this->lastMessage = self::create($wallMessage, $this->author)
                            ->setAttachments($wall['wall']['attachments'])
                            ->setText($countOfTypes > 1 ? null : $this->text)
                            ->setPeerId($this->peerId)
                            ->setLastMessage($this->lastMessage)
                            ->setIsWallPost(true)
                            ->setWallPostLink($wall['wall']['owner_id'], $wall['wall']['id'])
                            ->setWallPostText($wall['wall']['text'])
                            ->send();
                    }

                    continue;
                }

                if ($type === 'audios') {
                    $this->prepareAudioMedias($attachments);
                } elseif ($type === 'photos') {
                    $this->preparePhotoMedias($attachments);
                } elseif ($type === 'doc') {
                    $this->prepareDocMedias($attachments);
                }

                $params = [
                    'chat_id' => $tgChatId,
                    'media' => $attachments
                ];

                $lastMessageId = $this->lastMessage['message_id'] ?? $this->lastMessage[0]['message_id'] ?? null;

                if ($lastMessageId) {
                    $params['reply_to_message_id'] = $lastMessageId;
                }

                $this->lastMessage = Telegram::sendMediaGroup($params);
            }
        }

        return $this->lastMessage;
    }

    public function getTgChats($vkChatId)
    {
        $cacheId = 'vk.relation.tg_chat' . $vkChatId;

        return cache()->remember($cacheId, now()->addMinutes(60), function () use ($vkChatId) {
            $vkChat = VkChat::query()->firstWhere('chat_id', $vkChatId);

            return $vkChat->tgChats()->get();
        });
    }

    public function getAttachments(): array
    {
        if (empty($this->attachments)) {
            return [];
        }

        $medias = [];

        if ($data = $this->getWallPostAttachments()) {
            $medias['walls'] = $data;
        }

        if ($data = $this->getPhotoAttachments()) {
            $medias['photos'] = $data;
        }

        if ($data = $this->getAudioAttachments()) {
            $medias['audios'] = $data;
        }

        if ($data = $this->getDocAttachments()) {
            $medias['doc'] = $data;
        }

        return $medias;
    }

    public function getPhotoAttachments(): InputMediaPhoto|array
    {
        $attachments = $this->filterAttachmentsByType('photo');

        if (empty($attachments)) {
            return [];
        }

        $medias = [];

        foreach ($attachments as $attachment) {
            $sizes = $attachment['photo']['sizes'];

            $maxSize = Arr::last($sizes);

            $media = [
                'media' => $maxSize['url'],
                'type'  => 'photo',
                'parse_mode' => 'html'
            ];

            $medias[] = $media;
        }

        return $medias;
    }

    public function getAudioAttachments(): InputMediaAudio|array
    {
        $type = 'audio';

        $attachments = $this->filterAttachmentsByType($type);

        if (empty($attachments)) {
            return [];
        }

        $medias = [];

        foreach ($attachments as $attachment) {
            $audio = $attachment['audio'];

            $media = [
                'media' => $audio['url'],
                'type'  => $type,
                'parse_mode' => 'html'
            ];

            $medias[] = $media;
        }

        return $medias;
    }

    public function getDocAttachments(): InputMediaPhoto|array
    {
        $attachments = $this->filterAttachmentsByType('doc');

        if (empty($attachments)) {
            return [];
        }

        $medias = [];

        foreach ($attachments as $attachment) {
            $media = [
                'media' => $attachment['doc']['url'],
                'type'  => 'document',
                'parse_mode' => 'html'
            ];

            $medias[] = $media;
        }

        return $medias;
    }

    public function getWallPostAttachments(): array
    {
        return $this->filterAttachmentsByType('wall');
    }

    private function filterAttachmentsByType($type): array
    {
        return Arr::where($this->attachments, function ($attachment) use ($type) {
            return $attachment['type'] === $type;
        });
    }

    private function getWallPostText(): string
    {
        return $this->isWallPost ? $this->wallPostText : "";
    }

    private function getWallPostLink(): string
    {
        return $this->isWallPost && $this->wallPostLink ? $this->wallPostLink : "";
    }

    private function getWallPostInformation(): string
    {
        return $this->getWallPostText() . $this->getWallPostLink();
    }

    private function prepareAudioMedias(&$medias)
    {
        $attachments = $this->filterAttachmentsByType('audio');

        if (empty($attachments) || empty($medias)) {
            return;
        }

        $audioCaptions = [];

        foreach ($attachments as $attachment) {
            $audio = $attachment['audio'];

            $audioCaptions[] = $audio['artist'] . ' - ' . $audio['title']
                . ' (~'
                . Str::padLeft(floor($audio['duration'] / 60), 2, 0)
                . ':'
                . Str::padLeft($audio['duration'] % 60, 2, 0)
                . ')';
        }

        $caption = $this->part === 1
            ? $this->captionService->generate() . $this->getWallPostInformation()
            : "";

        $medias[0]['caption'] = $caption
            . "\n\n<b>Аудио:</b>\n" .  implode("\n", $audioCaptions);

        $this->part++;

        $medias = InputMediaAudio::make($medias);
    }

    private function preparePhotoMedias(&$medias): void
    {
        if (empty($medias)) {
            return;
        }

        if ($this->part === 1) {
            $medias[0]['caption'] = $this->captionService->generate() . $this->getWallPostInformation();
        }

        $this->part++;

        $medias = InputMediaPhoto::make($medias);
    }

    private function prepareDocMedias(&$medias): void
    {
        if (empty($medias)) {
            return;
        }

        if ($this->part === 1) {
            $medias[0]['caption'] = $this->captionService->generate() . $this->getWallPostInformation();
        }

        $this->part++;

        $medias = InputMediaDocument::make($medias);
    }
}
